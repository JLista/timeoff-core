/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu.mock_auth;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.mobile.device.Device;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.solu.security.jwt.JwtTokenUtil;
import com.solu.security.user.AuthUser;
import com.solu.security.user.AuthUserService;
import com.solu.util.SoluLogger;

/**
 * This controller provides a simple web service to set a user in the mock auth server
 * and provide the JWT token to the micro-service frontend app.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
@Controller
public class MainController {
  private static final SoluLogger LOG = SoluLogger.getLogger(MainController.class);

  //
  // Dependency Injection
  //

  @Autowired
  private AuthUserService userSvc;

  @Autowired
  private JwtTokenUtil jwtTokenUtil;
  
  /**
   * Shared JWT token.  Invoke the /api/users/set GET request to change the
   * current test user and then refresh the webapp in the other tab.
   */
  private String userJWT;
  
  private AuthUser user;

  //
  // REST services
  //

  /**
   * The webapp's Home page handler.
   * 
   * @return the symbolic name of the view to render
   */
  @RequestMapping(value = { "/api/users/set" }, method = RequestMethod.GET)
  public String setUser(
      // must have a user id parameter
      @RequestParam(name = "uid", required = true)
      final int userID,
      // pass in the HTTP response so we can add the JWT token
      final Model model,
      // what type of device made the request
      final Device device) {
    
    // look up dummy user
    final Optional<AuthUser> userOpt = userSvc.findById(userID);
    if (userOpt.isPresent()) {
      user = userOpt.get();
      LOG.logDebug("user is %s", user);
      userJWT = jwtTokenUtil.generateToken(user, device);
      model.addAttribute("message", "User set to " + user);
      model.addAttribute("token", userJWT);
      // redirect to the Angular live-coding (ng serve) session
      return "set-message";
      
    } else {
      final String message = String.format("User id=%d was not found.", userID);
      LOG.logError(message);
      throw new RuntimeException(message);
    }
  }

  /**
   * Retrieve the current user's JWT token.
   * 
   * @return  JWT token string
   * 
   * @throws RuntimeException when no user is logged in (should never happen)
   */
  @SuppressWarnings({ "rawtypes", "unchecked" })
  @RequestMapping(name = "/api/auth", method = GET)
  public ResponseEntity<String> getAuthToken() {
    LOG.logDebug("MainController.getAuthToken");
    if (userJWT == null) {
      return new ResponseEntity(HttpStatus.NOT_ACCEPTABLE);
    } else {
      return ResponseEntity.ok(userJWT);
    }
  }
}
