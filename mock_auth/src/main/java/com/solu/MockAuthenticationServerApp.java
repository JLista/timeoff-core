/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.web.support.SpringBootServletInitializer;

/**
 * The Spring Boot application that runs a mock authentication server.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
@SpringBootApplication
public class MockAuthenticationServerApp extends SpringBootServletInitializer {
  public static void main(String[] args) {
    SpringApplication.run(MockAuthenticationServerApp.class, args);
  }
}
