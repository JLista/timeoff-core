import { Injectable } from '@angular/core';
import { CanActivate, Router } from '@angular/router';

import { AuthenticationService } from './AuthenticationService';

@Injectable()
export class AuthGuard implements CanActivate {
    
    constructor(private router: Router,
        private authSvc: AuthenticationService) { }
    
    canActivate() {
        return this.authSvc.isAuthenticated();
    }
}