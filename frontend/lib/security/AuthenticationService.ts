import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { AuthHttp } from 'angular2-jwt';
import { Observable } from 'rxjs';

import { LangUtils } from '../common/LangUtils';
import { User } from './User';
import { AppService } from '../services/AppService';

/**
 * A singleton Service for handling user authentication concerns.
 */
@Injectable()
export class AuthenticationService {
  private static readonly UNKNOWN_RESPONSE_MSG: string = 'unknown response';

  /**
   * The Promise that user is authenticated (or not).
   */
  private authenticated: Promise<boolean> = new Promise<boolean>((resolve, reject) => {
    this.authenticationResolve = resolve;
  });
  /**
   * The function to resolve the authentication Promise.
   */
  private authenticationResolve: any;

  /**
   * The Observable JWT token.
   */
  private authTokenFuture : Observable<string> = null;

  /**
   * The Observable (aka Future) user object.
   */
  private userFuture : Observable<User> = null;

  /**
   * The actual user object.
   */
  private user : User = null;

  /**
   * Construct the AuthenticationService.
   * 
   * @param {Http}  The regular Angular HTTP service.
   * @param {AuthHttp}  The JWT-friendly HTTP service.
   * @param {AppService}  The internal Application service; used for build URLs.
   */
  constructor(private http: Http, private authHttp: AuthHttp, private appSvc: AppService) {
    // launch the authentication validation check when Angular builds this service
    appSvc.isInitialized()
      .then((isInitialized) => this.validateAuthentication());
  }

  /**
   * Get an observable User object that holds data about the current user.
   */
  public getCurrentUser() : Observable<User> {
    if (this.userFuture == null) {
      // create the HTTP request observable
      this.userFuture = this.authHttp.get(this.appSvc.buildPortalURL('/api/users/me'))
        .map(res => { this.user = new User(res.json()); return this.user; });
    }
    return this.userFuture.share();
  }
  
  /**
   * Get a Promise of whether the user is authenticated.
   */
  public isAuthenticated() : Promise<boolean> {
    return this.authenticated;
  }

  //
  // Private methods
  //

  /**
   * Internal method to query the Auth Server (Portal or Mock Auth Server) for the
   * user's JWT token.  This token is then stored in the browser's local store so that
   * it can be used by the AuthHttp service.
   */
  private validateAuthentication() {
    localStorage.removeItem('token');
    this.authTokenFuture = this.http.get(this.appSvc.buildPortalURL('/api/auth'))
    .map((response: any) => {
      if (typeof response === 'object') {
        // if the response is a redirect to the login page
        if (LangUtils.propertyEquals(response, 'url', this.appSvc.buildPortalURL('/login'))) {
          this.authenticationResolve(false);
          // then force the browser (not Ajax) to go back to the Login page
          this.redirectToLogin();
        } else {
          // valid response
          this.authenticationResolve(true);
          return response.text();
        }
      } else {
        console.error(AuthenticationService.UNKNOWN_RESPONSE_MSG, response);
        throw new Error(AuthenticationService.UNKNOWN_RESPONSE_MSG);
      }
    });
    // update the local storage when the JWT token returns
    this.authTokenFuture.share().subscribe(
    (token: string) => {
      localStorage.setItem('token', token);
    },
    (error) => {
      // TODO: better error handling infrastructure
      alert(error);
      localStorage.removeItem('token');
    });
  }

  /**
   * Internal method to query the Auth Server (Portal or Mock Auth Server) for the
   * user's JWT token.  This token is then stored in the browser's local store so that
   * it can be used by the AuthHttp service.
   */
  private redirectToLogin() {
    window.location.href = this.appSvc.buildPortalURL('/');
  }

}
