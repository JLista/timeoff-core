
/**
 * The frontend representation of the user roles.
 */
export class Role {

  /**
   * The code for the general User role.
   */
  public static readonly USER: string = "ROLE_USER";

  /**
   * The code for the general Administrator role.
   */
  public static readonly ADMIN: string = "ROLE_ADMIN";
}
