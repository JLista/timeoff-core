import { Role } from './Role';

/**
 * The frontend representation of the AuthUser entity.
 */
export class User {
  
  /** 
   * Make a new User object according to a provided JSON object.
   *
   * @param: json object containing the User information
   */
  public static make(json: any) {
    return new User(json);
  }

  public id: number = null;
  public firstName: string = null;
  public lastName: string = null;
  public fullName: string = null;
  public emailAddress: string = null;
  public roleCode: string = Role.USER;
  public allRoleCodes: string[];

  constructor(json: any) {
    Object.assign(this, json);
  }

  /**
   * Get the user's initials.
   */
  public getInitials(): string {
    return '' + this.firstName.charAt(0) + this.lastName.charAt(0);
  }

  /**
   *  Check if user has the provided role in their list of roles
   *
   *  @param roleCode the role to check against the user's list of roles
   */
  public hasRole(roleCode: string): boolean {
    return this.allRoleCodes.indexOf(roleCode) > -1;
  }

  /**
   *  Check if user has any of the provided roles in their list of roles
   *
   *  @param roleCodes the roles to check against the user's list of roles
   */
  public hasAnyRole(roleCodes: string[]): boolean {
    for(let anyRoleCode of roleCodes){
      if(this.hasRole(anyRoleCode)) return true;
    }
    return false;
  }
  
}
