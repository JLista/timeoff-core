import { NgModule } from '@angular/core';
import { APP_INITIALIZER } from '@angular/core';
import { CommonModule } from '@angular/common';


// Imports for configuring a JWT-authenticated HTTP service
import { HttpModule, Http, RequestOptions } from '@angular/http';
import { AuthHttp, AuthConfig } from 'angular2-jwt';

/**
 * Function to create the AuthHttp Service component.
 */
export function authHttpServiceFactory(http: Http, options: RequestOptions) {
  return new AuthHttp(new AuthConfig({ noClientCheck: true }), http, options);
}


// Services
import { ResizeService } from './resize/ResizeService';
import { AuthenticationService } from './security/AuthenticationService';
import { AuthGuard } from './security/AuthGuard';
import { AppService } from './services/AppService';

// Widgets
import { InitialSymbolComponent } from './widgets/initial-symbol.component';

// Utilities
import { DomUtils } from './common/DomUtils';

/**
 * The Angular module declaration for the Core FE library.
 */
@NgModule({
  declarations: [
    InitialSymbolComponent
  ],
  exports: [
    InitialSymbolComponent
  ],
  imports: [
    CommonModule
  ],
  providers: [ResizeService, AppService,
    // Security
    AuthenticationService, AuthGuard
    // HTTP
    ,{
      provide: AuthHttp,
      useFactory: authHttpServiceFactory,
      deps: [Http, RequestOptions]
    }
    // AppService initialization
    ,{
      provide: APP_INITIALIZER,
      useFactory: initializeAppService,
      multi: true,
      deps: [AppService]
    }
    ,{
      provide: APP_INITIALIZER,
      useFactory: checkAuthentication,
      multi: true,
      deps: [AppService, AuthenticationService]
    }
    ]
})
export class SoluCoreLibraryModule { }

export function initializeAppService(appSvc: AppService): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise((resolve, reject) => {
      let contextPath: string = DomUtils.getBodyAttribute('contextPath');
      let portalPath: string = DomUtils.getBodyAttribute('portalPath');
      appSvc.setPaths(contextPath, portalPath);
      resolve();
    });
  };
}

/**
 * A function that returns an Angular app initializer Promise.
 */
export function checkAuthentication(appSvc: AppService, authSvc: AuthenticationService): () => Promise<any> {
  return (): Promise<any> => {
    return new Promise((resolve, reject) => {
      // for Angular to wait until both the AppService is initialized
      appSvc.isInitialized()
        .then(() => {
            // and the user is authenticated
            authSvc.isAuthenticated().then(result => {
              if (result) { resolve(); } else { reject(); }
            });
        });
    });
  };
}

// Other library exports
export * from './common/DomUtils';
export * from './common/LangUtils';
export * from './resize/NativeEvent';
export * from './resize/ScreenMode';
export * from './resize/ResizeEvent';
export * from './resize/ResizeService';
export * from './security/User';
export * from './security/Role';
export * from './security/AuthenticationService';
export * from './security/AuthGuard';
export * from './services/AppService';
