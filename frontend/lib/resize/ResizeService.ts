import { Injectable } from '@angular/core';

import { Subject, Observable, Subscription } from 'rxjs';

import { NativeEvent } from './NativeEvent';
import { ScreenMode } from './ScreenMode';
import { ResizeEvent } from './ResizeEvent';

/**
 * A singleton service to provide browser resize events
 * to the rest of the application.
 */
@Injectable()
export class ResizeService {

  private static readonly PHONE_MAX_WIDTH: number = 560;
  private subject = new Subject<ResizeEvent>();
  private currentMode: ScreenMode = null;
  
  /**
   * Subscribe to resize events.
   * 
   * @param handler  an event handler function
   * 
   * @return {Subscription}  a subscription to the resize event stream
   */
  public subscribe(handler: any) : Subscription {
    // when subscription first occurs, calculate the initial screen size
    if (this.currentMode === null) {
      this.currentMode = this.calcScreenMode(window.innerWidth);
    }
    // subscript the handler
    return this.subject.asObservable().subscribe(handler);
  }

  /**
   * Get the current screen mode.
   * 
   * @return {ScreenMode}
   */
  public getCurrentMode() : ScreenMode {
    return this.currentMode;
  }

  /**
   * Fire a resize event to all listeners.
   * 
   * @param {NativeEvent}  the native browser event
   */
  public fire(event: NativeEvent) {
    let newMode: ScreenMode = this.calcScreenMode(event.target.innerWidth);
    // create the resize event as a raw JS object
    this.subject.next( <ResizeEvent> {nativeEvent: event, oldMode: this.currentMode, newMode: newMode});
    this.currentMode = newMode;
  }

  /**
   * Calculate the screen mode based upon the browser resize data.
   */
  private calcScreenMode(width: number) {
      if (width < ResizeService.PHONE_MAX_WIDTH) {
          return ScreenMode.Phone;
      } else {
          return ScreenMode.Desktop;
      }
  }
}
