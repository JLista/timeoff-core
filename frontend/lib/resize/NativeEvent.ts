export interface NativeEvent extends Event {
    target: NativeEventTarget;
}

export interface NativeEventTarget extends EventTarget {
    innerWidth: number
}
