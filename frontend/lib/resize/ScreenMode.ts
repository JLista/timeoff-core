export enum ScreenMode {
    Desktop,
    Tablet,
    Phone
}