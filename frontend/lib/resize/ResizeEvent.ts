
import { NativeEvent } from './NativeEvent';

import { ScreenMode } from './ScreenMode';

/**
 * An event that captures whenever a device browser is resized.
 */
export interface ResizeEvent {
    /**
     * The browser event of the window resize.
     */
    readonly nativeEvent: NativeEvent;

    /**
     * The old screen mode.
     */
    readonly oldMode: ScreenMode;

    /**
     * The new screen mode.
     */
    readonly newMode: ScreenMode;
}