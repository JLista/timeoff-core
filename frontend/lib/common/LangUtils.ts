/**
* @copyright 2017: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/


/**
 * Utilities on the TypeScript (and JavaScript) semantics.
 * Functions dealing with inspecting or manipulating objects, arrays, functions and so on.
 * 
 * @author Bryan Basham <bbasham@solutechnology.com>
 * 
 * @class
 */
export class LangUtils {

  /**
   * Determines if a reference is defined with a value.
   * 
   * @param {any} obj - reference to test
   * 
   * @return {boolean} true if the object exists
   */
  public static exists(obj: any): boolean {
    return obj !== null && obj !== undefined;
  }

  /**
   * Determines if a string reference is non-existent (ie, null) or empty.
   * A string is empty if it has no length.
   * 
   * @param {string} str The string to test
   * 
   * @return {boolean} true if the string is empty
   */
  public static isEmpty(str: string): boolean {
    return !LangUtils.exists(str) || str.length === 0;
  }

  /**
   * Determines if a string reference is non-existent (ie, null) or blank.
   * A string is blank if it is empty (no length) or if it contains only whitespace.
   * 
   * @param {string} str The string to test
   * 
   * @return {boolean} true if the string is blank
   */
  public static isBlank(str: string): boolean {
    return LangUtils.isEmpty(str) || str.trim().length === 0;
  }

  /**
   * Query if the object contains an attribute that is equal to an expected value.
   * 
   * @param {Object} obj  The object to inspect.
   * @param {string} propName  The name of the attribute to lookup in the object.
   * @param {any} expectedValue  The value expecting to find in the object.
   * 
   * @return {boolean} true if the attribute has that value
   */
  public static propertyEquals<T>(obj: Object, propName: string, expectedValue: T) {
    return LangUtils.exists(obj) && obj[propName] === expectedValue;
  }

}