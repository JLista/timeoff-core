/**
* Copyright 2017: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/


/**
 * Utilities for core DOM access and manipulation.
 * 
 * Functions dealing with inspecting or manipulating the raw DOM.
 * This should be used sparingly.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
export class DomUtils {

  private static readonly OPEN_MODAL_CLASS: string = 'open-modal';

  /**
   * Get the attribute value from the 'body' element.
   * 
   * @param {string} attrName - the name of the attribute to retrieve
   * 
   * @return {string} the attribute value
   */
  public static getBodyAttribute(attrName: string): string{
    return document.body.getAttribute(attrName);
  }

  /**
   * Set the screen to modal mode.
   */
  public static setModal() {
    document.body.classList.add(DomUtils.OPEN_MODAL_CLASS);
  }

  /**
   * Un-set the screen to modal mode.
   */
  public static unsetModal() {
    document.body.classList.remove(DomUtils.OPEN_MODAL_CLASS);
  }

}