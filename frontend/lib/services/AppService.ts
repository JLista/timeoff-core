/**
* @copyright 2017: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/

import { Injectable } from '@angular/core';

/**
 * A singleton Service for handling application-level actions.
 * 
 * @author Bryan Basham <bbasham@solutechnology.com>
 * 
 * @class
 */
@Injectable()
export class AppService {

  private backendPath: string = null;
  private authServerPath: string = null;

  /**
   * The Promise that this service is initialized.
   */
  private initialized: Promise<boolean> = new Promise<boolean>((resolve, reject) => {
    this.initializeResolve = resolve;
  });

  /**
   * The function to resolve the initialization Promise.
   */
  private initializeResolve: (boolean) => void;

  /**
   * Build the AppService.
   */
  constructor() { }
  
  /**
   * Set the application paths.
   * 
   * This method initializes thie service.
   * 
   * @param {string} contextPath - The webapp path to the micro-service application.
   * @param {string} authServerPath - The webapp path to the Portal (Auth Server) application.
   */
  public setPaths(contextPath: string, authServerPath: string) {
    if (this.backendPath !== null) {
      throw new Error("The paths must be set only once.");
    }
    // strip off the trailing slash of the URL
    contextPath = contextPath.substring(0, contextPath.length-1);
    this.backendPath = contextPath;
    // TODO: more infrastructure needed when Portal has a non-ROOT context path
    // but for now we'll use the CM service context path to extract the ROOT URL
    let portalPath = contextPath.substring(0, contextPath.lastIndexOf('/'));
    // on AWS servers the port portion (that should always be port 80) must be removed
    if (portalPath.endsWith(':80')) {
      portalPath = portalPath.substring(0, portalPath.length - 3);
    }
    this.authServerPath = portalPath;
    // resolve the initialized promise
    this.initializeResolve(true);
  }

  /**
   * Query whether this service has been initialized.
   * 
   * @return {Promise} an async promise of the initialization state.
   */
  public isInitialized(): Promise<boolean> {
    return this.initialized;
  }

  /**
   * Build a URL from a relative URL.
   * 
   * @param {string} relativeURL - The path within the micro-service application.
   * 
   * @return {string] The full URL to the resource.
   */
  public buildURL(relativeURL: string): string {
    if (this.backendPath === null) {
      throw new Error("The backendPath has not been set, yet.");
    }
    return this.backendPath + relativeURL;
  }

  /**
   * Build a Portal URL from a relative URL.
   * 
   * @param {string} relativeURL - The path within the Portal application.
   * 
   * @return {string] The full URL to the Portal API.
   */
  public buildPortalURL(relativeURL: string): string {
    if (this.authServerPath === null) {
      throw new Error("The authServerPath has not been set, yet.");
    }
    return this.authServerPath + relativeURL;
  }
}

