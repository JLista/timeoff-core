# Solu Core Libraries

This Maven project encapsulates the core, shared libraries for both backend and frontend.

It is organized into two modules:

* The `backend` module provides the BE utilities and critical business abstractions.
* The `frontend` module provides the FE utilities and critical UI abstractions.

There is also a Maven module called `mock_auth` that provides a mock Authentication Service for use only in **Dev Mode**.