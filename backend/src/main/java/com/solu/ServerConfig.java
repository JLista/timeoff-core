/**
* Copyright 2015 - 2016: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu;


/**
 * A simple data holder that contains information unique to the deployment of
 * the SMTng webapp.  This API is used to generate absolute URLs from relative
 * URLs; typically, used to generate links for email messages.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
public final class ServerConfig {

  /**
   * The webapp's build version.
   */
  public final String appVersion;

  /**
   * The webapp's context path.
   */
  public final String contextPath;

  /**
   * The webapp's host name.
   */
  public final String serverHost;

  /**
   * The webapp's port.
   */
  public final int serverPort;
  
  /**
   * Constructor; used by the {@link Application} component.
   */
  public ServerConfig(
      final String appVersion,
      final String contextPath,
      final String serverHost,
      final int serverPort) {
    this.appVersion = appVersion;
    this.contextPath = contextPath;
    this.serverHost = serverHost;
    this.serverPort = serverPort;
  }
  
  /**
   * Make an absolute URL path from a relative URL path by prepending the
   * webapp's server information.
   * 
   * @param relativeURL  A relative URL path, eg {@code "auth/resetPS"}
   * 
   * @return An absolute URL path, eg {@code "http://localhost:8080/SMTng-bdb/auth/resetPS"}
   */
  public String makeURL(final String relativeURL) {
    return String.format("http://%s:%s/%s/%s", serverHost, serverPort, contextPath, relativeURL);
  }
}
