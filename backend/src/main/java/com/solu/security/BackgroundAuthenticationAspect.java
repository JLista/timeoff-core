/**
* Copyright 2015 - 2015: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.security;

import java.lang.reflect.Method;
import java.util.Arrays;
import java.util.HashSet;
import java.util.Set;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.google.common.base.Joiner;
import com.solu.domain.Role;
import com.solu.util.SoluLogger;

/**
 * The BackgroundAuthenticationAspect class provides an Aspect that wraps around
 * background Service tier methods that are annotated with
 * {@link RunAsSecurityRole} to inject the Spring Security authentication
 * context.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
@Component
@Aspect
public class BackgroundAuthenticationAspect {
  private static final SoluLogger LOG = SoluLogger.getLogger(BackgroundAuthenticationAspect.class);

  @Autowired
  private SecurityUtils securityUtils;

  /**
   * Handler for the around-aspect on methods with the {@link RunAsSecurityRole} annotation.
   * 
   * @param pjp the wrapped method's join point
   * 
   * @return the value returned by the wrapped method
   * 
   * @throws Throwable any exception thrown by the wrapped method
   */
  @Around("@annotation(com.solu.security.RunAsSecurityRole)")
  public Object authenticateForMethod(final ProceedingJoinPoint pjp) throws Throwable {
    try {
      final Set<Role> roles = getRolesOnTargetMethod(pjp);
      LOG.logTrace("Secure the method %s with role(s) %s", getMethodName(pjp), Joiner.on(',').join(roles));
      // inject the SYSTEM user into the security context
      securityUtils.assignSystemUser(roles);
      // call the wrapped method
      return pjp.proceed();

    } finally {
      // clean up security context changes
      securityUtils.removeSystemUser();
    }
  }

  /**
   * Gets the set of {@linkplain Role roles} listed in the method's {@link RunAsSecurityRole} annotation.
   * 
   * @param pjp the wrapped method's join point
   * 
   * @return the set of {@linkplain Role roles}
   * 
   * @throws Exception when any of the Java reflection code fails
   */
  private Set<Role> getRolesOnTargetMethod(final ProceedingJoinPoint pjp) throws Exception {
    final MethodSignature signature = (MethodSignature) pjp.getSignature();
    final String name = signature.getMethod().getName();
    final Class<?>[] parameterTypes = signature.getMethod().getParameterTypes();
    final Class<?> targetClass = pjp.getTarget().getClass();
    final Method targetMethod = targetClass.getDeclaredMethod(name, parameterTypes); //signature.getMethod();
    final RunAsSecurityRole annotation = targetMethod.getAnnotation(RunAsSecurityRole.class);
    final Role[] roles = annotation.value();
    final Set<Role> allRoles = new HashSet<>();
    Arrays.stream(roles).forEach(role -> {
      do {
        allRoles.add(role);
        role = role.getRoleSubsumed();
      } while (role != null);
    });
    return allRoles;
  }

  /**
   * Gets the name (as a Class.Method pair) of the wrapped method.
   * 
   * @param pjp the wrapped method's join point
   * 
   * @return the name of the wrapped method
   */
  private String getMethodName(final ProceedingJoinPoint pjp) {
    MethodSignature signature = (MethodSignature) pjp.getSignature();
    return signature.getDeclaringTypeName() + "." + signature.getName();
  }
}
