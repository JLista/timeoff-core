/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu.security.jwt;

import java.io.IOException;

import javax.servlet.FilterChain;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.core.env.Environment;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.web.authentication.WebAuthenticationDetailsSource;
import org.springframework.web.filter.OncePerRequestFilter;

import com.solu.SoluProfile;
import com.solu.security.WebResourceUtils;
import com.solu.util.SoluLogger;

/**
 * Filter all HTTP requests and verify the JWT token.
 * 
 * <p>
 * This filter assumes that the webapp user has already been authenticated by
 * the Portal. If no JWT token was found or if the token can't be verified then
 * an Unauthenticated status is returned with a redirect back to the portal.
 */
public class JwtAuthenticationTokenFilter extends OncePerRequestFilter {
  private static final SoluLogger LOG = SoluLogger.getLogger(JwtAuthenticationTokenFilter.class);

  //
  // Injected Dependencies
  //
  
  @Autowired
  private UserDetailsService userDetailsService;

  @Autowired
  private JwtTokenUtil jwtTokenUtil;

  @Value("${jwt.header}")
  private String tokenHeader;
  
  @Autowired
  private Environment environment;
  
  @Autowired
  private WebResourceUtils webResourcesUtils;

  //
  // OncePerRequestFilter methods
  //

  /**
   * Ensure a valid JWT token is provided on the HTTP request.
   * Otherwise, redirect the client back to the Portal.
   * 
   * {@inheritDoc}
   */
  @Override
  protected void doFilterInternal(
      final HttpServletRequest request,
      final HttpServletResponse response,
      final FilterChain chain)
      throws ServletException, IOException {
    
    // skip over web resources (always serve up static files)
    if (! webResourcesUtils.isResourceURL(request)) {
      
      LOG.logDebug("request URL: " + request.getRequestURI());
      // verify that the token header exists
      final String authHeader = request.getHeader(tokenHeader);
      if (authHeader == null) {
        LOG.logError("No JWT token found in the HTTP request.");
        handleUnauthorized(response);
        return;
      }

      // extract the token from the header
      final String authToken =
          // extract just the JWT token portion of the header value (remove 'Bearer ' if present)
          (authHeader.startsWith("Bearer ")) ? authHeader.substring(7)
              : authHeader;

      // verify the token
      String username = jwtTokenUtil.getUsernameFromToken(authToken);
      if (username == null) {
        LOG.logError("Could not extract the username from the JWT token.");
        handleUnauthorized(response);
        return;
      }

      // set (or verify) the Spring Security context
      final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
      if (auth == null) {
        setAuth(username, authToken, request);
      } else {
        if (!auth.getName().equals(username)) {
          if (environment.acceptsProfiles(SoluProfile.DEV)) {
            // in Dev Mode always switch to the newly created JWT token
            setAuth(username, authToken, request);
          } else {
            // TODO: what else should we do?
            LOG.logError("Token username (%s) doesn't match current principal (%s).", username, auth.getName());
          }
        }
      }
    }

    // if we got this far we can send the HTTP request up the chain
    chain.doFilter(request, response);
  }

  //
  // Private methods
  //

  /**
   * Reject the request with a 401 status.
   */
  private void handleUnauthorized(HttpServletResponse response) throws IOException {
    response.sendError(HttpServletResponse.SC_UNAUTHORIZED);
  }
  
  /**
   * Set the authentication principal into the Spring Security framework.
   */
  private void setAuth(String username, String authToken, HttpServletRequest request) {
    // It is not compelling necessary to load the use details from the
    // database. You could also store the information
    // in the token and read it from it. It's up to you ;)
    UserDetails userDetails = userDetailsService.loadUserByUsername(username);

    // For simple validation it is completely sufficient to just check the
    // token integrity. You don't have to call
    // the database compellingly. Again it's up to you ;)
    if (jwtTokenUtil.validateToken(authToken, userDetails)) {
      LOG.logDebug("authenticated user " + userDetails.getUsername() + ", setting security context");
      UsernamePasswordAuthenticationToken authentication =
          new UsernamePasswordAuthenticationToken(
              userDetails, null, userDetails.getAuthorities());
      authentication.setDetails(new WebAuthenticationDetailsSource().buildDetails(request));
      SecurityContextHolder.getContext().setAuthentication(authentication);
      
    } else {
      LOG.logError("Token validation failed.");
      throw new IllegalStateException("Invalid JWT token.");
    }
  }
}