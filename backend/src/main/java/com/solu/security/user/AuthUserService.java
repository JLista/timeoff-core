/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu.security.user;

import static com.solu.util.Preconditions.checkArgument;
import static com.solu.util.Preconditions.checkNotNull;
import static com.solu.util.ValidationMessages.USER_NOT_NULL;

import java.util.Date;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.solu.domain.DomainEntity;

/**
 * The Service to manage the Portal {@linkplain AuthUser application users}.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
@Service
public class AuthUserService {

  @Autowired
  private AuthUserDAO userDAO;

  /**
   * Find the {@linkplain AuthUser user} with a database id.
   * 
   * <p>
   * This service does not require authentication.
   * </p>
   * 
   * @param userID  database id of a user
   * 
   * @return an optional {@link AuthUser}; empty if no user with this emailAddr is found
   */
  @Transactional(readOnly = true)
  public Optional<AuthUser> findById(int userID) {
    return Optional.ofNullable(userDAO.findOne(userID));
  }

  /**
   * Find the {@linkplain AuthUser user} with this email address.
   * 
   * <p>
   * This service does not require authentication.
   * </p>
   * 
   * @param emailAddr email address of a user
   * 
   * @return an optional {@link AuthUser}; empty if no user with this emailAddr is found
   */
  @Transactional(readOnly = true)
  public Optional<AuthUser> findByEmailAddress(final String emailAddr) {
    return userDAO.findByEmailAddress(checkNotNull(emailAddr, "emailAddr must not be null"));
  }

  /**
   * Record that the {@linkplain AuthUser user} started a new session.
   * 
   * @param user the {@link AuthUser} that has just logged in
   * 
   * @throws NullPointerException
   *    when the {@code user} parameter is {@code null}
   * @throws IllegalArgumentException
   *    when the {@link AuthUser} entity <em>doesn't</em> have a DB id
   */
  @Transactional(readOnly = false)
  public void recordSession(final AuthUser user) {
    checkNotNull(user, USER_NOT_NULL);
    checkArgument(!user.isNew(), DomainEntity.ENTITY_DOESNT_EXIST);
    //
    final AuthUser userEntity = userDAO.findOne(user.getId());
    userEntity.setLastSession(new Date());
    userDAO.saveAndFlush(userEntity);
  }

}
