/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu.security.user;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

/**
 * The repository for the user entity used for security.
 * 
 * @author <a href='mailto:basham47@gmail.com'>Bryan Basham</a> 
 */
@Repository
public interface AuthUserDAO extends JpaRepository<AuthUser, Integer> {

  Optional<AuthUser> findByEmailAddress(String email);

}
