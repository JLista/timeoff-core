/**
* Copyright 2015 - 2016: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.security.user;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import java.util.List;

import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.solu.domain.Role;

/**
 * The RESTful service for {@link Role} values.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
@RestController
@RequestMapping(value = "/api/roles")
public class AuthUserRoleController {

  /**
   * Retrieve a complete list of {@linkplain Role user roles}.
   * 
   * @return  JSON representation of the user roles
   */
  @RequestMapping(method = GET)
  public List<Role> listAll() {
    return Role.getAll();
  }

}
