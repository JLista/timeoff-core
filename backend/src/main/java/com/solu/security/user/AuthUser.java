/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu.security.user;

import java.util.Collection;
import java.util.Date;
import java.util.stream.Collectors;

import javax.persistence.Entity;
import javax.persistence.Table;
import javax.persistence.Transient;

import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.social.security.SocialUserDetails;

import com.solu.domain.AbstractUser;

/**
 * Concrete class to represent users in the Portal.
 * 
 * @author <a href='mailto:basham47@gmail.com'>Bryan Basham</a> 
 */
@Entity
@Table(name = "person")
public class AuthUser extends AbstractUser implements SocialUserDetails {
  private static final long serialVersionUID = -8676454615405415565L;

  @Transient
  private Collection<? extends GrantedAuthority> authorities = null;

  //
  // UserDetails (Spring Security) methods
  //

  /**
   * {@inheritDoc}
   */
  @Transient
  @Override
  public Collection<? extends GrantedAuthority> getAuthorities() {
    if (authorities == null) {
      authorities = getAllRoles().stream()
          .map(Enum::name)
          .map(SimpleGrantedAuthority::new)
          .collect(Collectors.toList());
    }
    return authorities;
  }

  /**
   * {@inheritDoc}
   */
  @Transient
  @Override
  public String getUsername() {
    return getEmailAddress();
  }

  /**
   * {@inheritDoc}
   */
  @Transient
  @Override
  public boolean isAccountNonExpired() {
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Transient
  @Override
  public boolean isAccountNonLocked() {
    return true;
  }

  /**
   * {@inheritDoc}
   */
  @Transient
  @Override
  public boolean isCredentialsNonExpired() {
    return true;
  }

  /**
   * This is used by the JWT components; not currently being used.
   */
  @Transient
  public Date getLastPasswordResetDate() {
    // TODO future expansion of JWT authentication
    return null;
  }

  //
  // SocialUserDetails (Spring Social) methods
  //

  /**
   * {@inheritDoc}
   */
  @Override
  public String getUserId() {
    return getUsername();
  }

  //
  // Object methods
  //

  /**
   * {@inheritDoc}
   */
  @Override
  public String toString() {
    return toStringHelper(AuthUser.class)
        .add("email", getEmailAddress())
        .add("role", getRoleCode())
        .toString();
  }
}
