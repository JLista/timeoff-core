/**
* Copyright 2015 - 2016: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.security.user;

import static org.springframework.web.bind.annotation.RequestMethod.GET;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Profile;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.annotation.JsonView;
import com.solu.SoluProfile;
import com.solu.domain.AbstractUser;
import com.solu.security.SecurityUtils;

/**
 * The RESTful service for the authentication server.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
@RestController
@RequestMapping(value = "/api/users")
// don't include this controller in the Mock Auth server
@Profile("!"+SoluProfile.MOCK_AUTH)
public class AuthUserController {

  //
  // Dependency Injection
  //

  @Autowired
  private SecurityUtils securityUtils;

  //
  // REST handlers
  //

  /**
   * Retrieve the current user (via Spring Security).
   * 
   * @return  JSON representation of the user
   * 
   * @throws RuntimeException when no user is logged in (should never happen)
   */
  @RequestMapping(value = "/me", method = GET)
  @JsonView(AbstractUser.UserView.class)
  public AbstractUser currentUser() {
    return securityUtils.getCurrentUser()
        .orElseThrow(() -> new RuntimeException("Could not identify the user."));
  }

}
