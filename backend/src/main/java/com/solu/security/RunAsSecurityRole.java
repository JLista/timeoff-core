/**
* Copyright 2015 - 2015: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.security;

import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;
import java.lang.annotation.Target;

import com.solu.domain.Role;

/**
 * The RunAsSecurityRole annotation provides a tool to specify an Aspect around
 * background Service tier methods that need Spring Security authentication.
 * This is used so that background processes can operate with a specified role.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
@Retention(RetentionPolicy.RUNTIME)
@Target(ElementType.METHOD)
public @interface RunAsSecurityRole {

  /**
   * The specific Role to which this background process must be authenticated for.
   */
  Role[] value();

}
