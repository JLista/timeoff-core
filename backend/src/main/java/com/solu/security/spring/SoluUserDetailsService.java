/**
* Copyright 2015 - 2015: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.security.spring;

import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Component;

import com.solu.security.user.AuthUser;
import com.solu.security.user.AuthUserService;
import com.solu.util.SoluLogger;

/**
 * Spring security hook for local authentication.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
@Component
public class SoluUserDetailsService implements UserDetailsService {
  private final SoluLogger LOG = SoluLogger.getLogger(SoluUserDetailsService.class);

  //
  // Dependency Injection
  //

  @Autowired
  private AuthUserService userSvc;

  //
  // UserDetailsService methods
  //

  @Override
  public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
    try {
      Optional<AuthUser> userOpt = userSvc.findByEmailAddress(username);
      if (userOpt.isPresent()) {
        final AuthUser user = userOpt.get();
        // fail if the user has been disabled
        if (!user.isEnabled()) {
          LOG.logWarning(String.format("%s attempted login but is disabled.", user));
          throw new DisabledException("User not enabled.");
        }
        // otherwise, record this session
        userSvc.recordSession(user);
        // and return a Spring Security user object
        return user;

      } else {
        final String msg = String.format("No user found for '%s'", username);
        LOG.logDebug(msg);
        throw new UsernameNotFoundException(msg);
      }

    } catch (DataAccessException e) {
      LOG.logError(e, "ERROR: '%s'", e.getMessage(), e);
      throw e;
    }
  }
}
