/**
* Copyright 2015 - 2015: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.security;

import java.util.Collection;
import java.util.Optional;
import java.util.Set;
import java.util.stream.Collectors;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.stereotype.Component;

import com.solu.domain.Role;
import com.solu.security.user.AuthUser;
import com.solu.security.user.AuthUserService;
import com.solu.util.SoluLogger;

/**
 * The SecurityUtils class provides a Utility class to handle internal security
 * concerns. In particular, this is useful for assigning a system user with a
 * specific role for background tasks such as Quartz scheduled tasks.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
@Component
public class SecurityUtils {
  private static final SoluLogger LOG = SoluLogger.getLogger(SecurityUtils.class);

  @Autowired
  private AuthUserService userSvc;

  /**
   * Gets an optional {@linkplain AuthUser user} for the current Web user.
   * 
   * @return {@link Optional#empty()} if no user is available,
   *    such as when a background process is being invoked.
   */
  public Optional<AuthUser> getCurrentUser() {
    final Authentication auth = SecurityContextHolder.getContext().getAuthentication();
    LOG.logDebug("auth: %s", auth);
    if (auth != null) {
      final Object principal = auth.getPrincipal();
      LOG.logDebug("principal: %s", principal);
      if (principal != null && principal instanceof AuthUser) {
        final AuthUser user = (AuthUser) principal;
        LOG.logDebug("user: %s", user);
        return userSvc.findByEmailAddress(user.getEmailAddress());
      }
    }
    return Optional.empty();
  }

  /**
   * Inject a {@linkplain Person user} into the Spring Security context.
   * 
   * @param user  The Solu {@linkplain Person user} that has just registered.
   */
  public void logInUser(final AuthUser user) {
    final Authentication authentication = new UsernamePasswordAuthenticationToken(user, null, user.getAuthorities());
    SecurityContextHolder.getContext().setAuthentication(authentication);
  }

  /**
   * Assign the SYSTEM user with the specified roles to the current thread's
   * security context.
   * 
   * <p>
   * This should only be used by background processes for
   * which a specific user doesn't exist.
   * </p>
   * 
   * <p>
   * Typical usage pattern looks like this:
   * <pre>
   * try {
   *   // establish SYSTEM user roles
   *   SecurityUtils.assignSystemUser(Sets.newHashSet(Role.ROLE_STAFF));
   *   // perform authorized operation
   *   staffServices.generateReports();
   *   
   * } finally {
   *   // remove SYSTEM user roles
   *   SecurityUtils.removeSystemUser();
   * }
   * </pre>
   * </p>
   * 
   * @param roles the set of {@linkplain Role roles} to be assigned to the system user
   */
  public void assignSystemUser(final Set<Role> roles) {
    final Collection<GrantedAuthority> authorities =
        roles.stream().map(Enum::name).map(SimpleGrantedAuthority::new).collect(Collectors.toList());
    final UsernamePasswordAuthenticationToken token =
        new UsernamePasswordAuthenticationToken(SYSTEM_USERNAME, null, authorities);
    SecurityContextHolder.getContext().setAuthentication(token);
  }

  /**
   * Remove the SYSTEM user from the current thread's security context. This
   * should only be used by background processes to undo the
   * {@link #assignSystemUser(Set)} method.
   */
  public void removeSystemUser() {
    SecurityContextHolder.getContext().setAuthentication(null);
  }

  //
  // Private methods
  //

  private static final String SYSTEM_USERNAME = "SYSTEM";

}
