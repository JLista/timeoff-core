/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu.security;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.http.HttpMethod;
import org.springframework.security.web.util.matcher.AntPathRequestMatcher;
import org.springframework.stereotype.Component;

import com.solu.util.SoluLogger;

/**
 * A utility component for handling HTTP requests for web resources.
 * 
 * @author <a href='mailto:basham47@gmail.com'>Bryan Basham</a> 
 */
@Component
public class WebResourceUtils {
  private static final SoluLogger LOG = SoluLogger.getLogger(WebResourceUtils.class);

  @Value("${solu.web.resourcePatternList}")
  private String[] resourcePatternList;
  
  private List<AntPathRequestMatcher> resourceMatchers;

  @PostConstruct
  private void init() {
    LOG.logDebug("resourcePatternList: %s", Arrays.toString(resourcePatternList));
    resourceMatchers = new ArrayList<>(resourcePatternList.length);
    for (final String pattern : resourcePatternList) {
      resourceMatchers.add(new AntPathRequestMatcher(pattern, HttpMethod.GET.name()));
    }
  }
  
  /**
   * Queries whether the {@code request} parameter matches one of the resource URL patterns.
   * 
   * @param request  the {@link HttpServletRequest} being tested
   * 
   * @return {@code true} if the request is a resource; otherwise {@code false}
   */
  public boolean isResourceURL(final HttpServletRequest request) {
    return resourceMatchers.stream()
        .anyMatch((matcher) -> matcher.matches(request));
  }

  public String[] getPatterns() {
    return resourcePatternList;
  }
}
