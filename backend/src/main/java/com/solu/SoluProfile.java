/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu;

/**
 * The enumeration of Spring profiles used by Solu internal projects.
 * 
 * @author <a href='mailto:basham47@gmail.com'>Bryan Basham</a> 
 */
public final class SoluProfile {
  public static final String DEV = "dev";
  public static final String TEST = "test";
  public static final String UAT = "uat";
  public static final String PROD = "prod";
  
  public static final String MOCK_AUTH = "mock_auth";
  
  private SoluProfile() {}
}
