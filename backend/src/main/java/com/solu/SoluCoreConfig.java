/**
 * Copyright 2015 - 2017: Solu Technology Partners
 * All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
 * Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.EnableAspectJAutoProxy;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.encrypt.Encryptors;
import org.springframework.security.crypto.encrypt.TextEncryptor;
import org.springframework.security.crypto.password.PasswordEncoder;

/**
 * Spring configuration bean that uses auto-configuration.
 * 
 * @author <a href='mailto:basham47@gmail.com'>Bryan Basham</a> 
 */
@Configuration
// scan this library for Spring components
@ComponentScan("com.solu")
@EnableAutoConfiguration
// enable Spring AOP
@EnableAspectJAutoProxy(proxyTargetClass = true)
public class SoluCoreConfig {

  @Value("${solu.security.pwEncoderStrength}")
  private int pwEncoderStrength;

  /**
   * Makes a {@link PasswordEncoder} Spring bean.
   * 
   * @return the {@link PasswordEncoder} bean
   */
  @Bean
  public PasswordEncoder passwordEncoder() {
    return new BCryptPasswordEncoder(pwEncoderStrength);
  }

  /**
   * Makes a {@link TextEncryptor} Spring bean.
   * 
   * @return the {@link TextEncryptor} bean
   */
  @Bean
  public TextEncryptor textEncryptor() {
    // TODO: user better encryption
    // see: http://docs.spring.io/spring-social/docs/1.0.x/reference/html/connecting.html
    return Encryptors.noOpText();
  }

}
