/**
* Copyright 2015 - 2016: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.util;

import static com.solu.util.ValidationMessages.*;

import java.util.regex.Pattern;

import javax.annotation.Nullable;

/**
 * Static convenience methods that help a method or constructor check whether it was invoked
 * correctly (whether its <i>preconditions</i> have been met).
 * 
 * <p>Based upon the Guava {@link com.google.common.base.Preconditions} class.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
public final class Preconditions {

  /**
   * Ensures the truth of an expression involving one or more parameters to the calling method.
   *
   * @param expression a boolean expression
   * @param errorMessage the exception message to use if the check fails; will be converted to a
   *     string using {@link String#valueOf(Object)}
   *     
   * @throws IllegalArgumentException if {@code expression} is false
   */
  public static void checkArgument(boolean expression, Object errorMessage) {
    com.google.common.base.Preconditions.checkArgument(expression, errorMessage);
  }

  /**
   * Ensures the truth of an expression involving one or more parameters to the calling method.
   *
   * @param expression a boolean expression
   * @param errorMessageTemplate a template for the exception message should the check fail. The
   *     message is formed by replacing each {@code %s} placeholder in the template with an
   *     argument. These are matched by position - the first {@code %s} gets {@code
   *     errorMessageArgs[0]}, etc.  Unmatched arguments will be appended to the formatted message
   *     in square braces. Unmatched placeholders will be left as-is.
   * @param errorMessageArgs the arguments to be substituted into the message template. Arguments
   *     are converted to strings using {@link String#valueOf(Object)}.
   *     
   * @throws IllegalArgumentException if {@code expression} is false
   * @throws NullPointerException if the check fails and either {@code errorMessageTemplate} or
   *     {@code errorMessageArgs} is null (don't let this happen)
   */
  public static void checkArgument(boolean expression,
      @Nullable String errorMessageTemplate,
      @Nullable Object... errorMessageArgs) {
    com.google.common.base.Preconditions.checkArgument(expression, errorMessageTemplate, errorMessageArgs);
  }

  /**
   * Ensures that an object reference passed as a parameter to the calling method is not null.
   *
   * @param reference an object reference
   * @param errorMessage the exception message to use if the check fails; will be converted to a
   *     string using {@link String#valueOf(Object)}
   *     
   * @return the non-null reference that was validated
   * 
   * @throws NullPointerException if {@code reference} is null
   */
  public static <T> T checkNotNull(T reference, Object errorMessage) {
    return com.google.common.base.Preconditions.checkNotNull(reference, errorMessage);
  }

  /**
   * Ensures that a string is not greater than a maximum length.
   *
   * @param string  The string whose length to be checked.
   * @param length  The maximum length of the string.
   * @param elementName  The name of this string; used to build an error message
   * 
   * @throws NullPointerException if {@code string} or {@code elementName} is null
   * @throws IllegalArgumentException if the {@code string}'s length is greater than the {@code length}
   */
  public static void checkTooLong(final String string, final int length, final String elementName) {
    checkArgument(!(string.length() > length), makeTooLongMessage(elementName));
  }

  /**
   * Ensures that a string is not smaller than a minimum length.
   *
   * @param string  The string whose length to be checked.
   * @param length  The minimum length of the string.
   * @param elementName  The name of this string; used to build an error message
   * 
   * @throws NullPointerException if {@code string} or {@code elementName} is null
   * @throws IllegalArgumentException if the {@code string}'s length is less than the {@code length}
   */
  public static void checkTooShort(final String string, final int length, final String elementName) {
    checkArgument(!(string.length() < length), makeTooShortMessage(elementName));
  }

  /**
   * Ensures that a string contains only digits.
   *
   * @param string  The string whose contents to be checked.
   * @param elementName  The name of this string; used to build an error message
   * 
   * @throws NullPointerException if {@code string} or {@code elementName} is null
   * @throws IllegalArgumentException if the {@code string}'s content contains non-digits
   */
  public static void checkNonDigits(final String string, final String elementName) {
    checkArgument(ALL_DIGITS.matcher(string).matches(), elementName + " contains non-digits");
  }

  //
  // Private
  //

  private static final Pattern ALL_DIGITS = Pattern.compile("\\d*");

  /* hide ctor to complete the Utility idiom */
  private Preconditions() {}
}
