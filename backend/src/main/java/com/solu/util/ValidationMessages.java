/**
* Copyright 2015 - 2016: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.util;

/**
 * Constant String literals used for validating error messages (caught when an error was intentional for a test)
 * 
 * @author <a href='mailto:scerbone@solutechnology.com'>Stephen Cerbone</a>
 */
public class ValidationMessages {

  //
  // Generic validation messages
  //

  /**
   * Make a validation message that the requested {@code parameter} is null.
   */
  public static String makeNotNullMsg(final String parameter) {
    return parameter + " must not be null";
  }

  //
  // Domain Objects
  //

  /** Message when a {@code name} parameter is null. */
  public static final String NAME_NOT_NULL = "name must not be null";

  /** Message when a {@code description} parameter is null. */
  public static final String DESCRIPTION_NOT_NULL = "description must not be null";

  //
  // AssessmentAssignment
  //

  /** Message when a {@code assessment} parameter is null. */
  public static final String ASSESSMENT_NOT_NULL = "assessment must not be null";

  /** Message when a {@code assessment} parameter does not exist in the database. */
  public static final String ASSESSMENT_DOESNT_EXIST = "assessment must have a key";

  //
  // Person
  //

  /** Message when a {@code recruiter} parameter is null. */
  public static final String RECRUITER_DOESNT_EXIST = "recruiter doesn't exist";

  /** Message when a {@code user} parameter is null. */
  public static final String USER_NOT_NULL = "user must not be null";

  /** Message when a {@code role} parameter is null. */
  public static final String ROLE_NOT_NULL = "role must not be null";

  /** Message when a {@code emailAddress} parameter is null. */
  public static final String EMAIL_NOT_NULL = "emailAddress must not be null";

  /** Message when a {@code lastSession} parameter is null. */
  public static final String LASTSESSION_NOT_NULL = "lastSession must not be null";

  /** Message when a {@code resourceType} parameter is null. */
  public static final String RESOURCE_TYPE_NOT_NULL = "resourceType must not be null";

  /** Message when a {@code resourceType} parameter does not exist in the database. */
  public static final String RESOURCE_TYPE_DOESNT_EXIST = "resourceType must have a key";

  //
  // AssessmentEmailSettings
  //

  /** Message when a {@code users} parameter is null. */
  public static final String USERS_NOT_NULL = "users must not be null";

  /** Message when a {@code users} parameter is non-null but blank. */
  public static final String USERS_NOT_EMPTY = "users must not be empty";

  /** Message when a {@code assignments} parameter is null. */
  public static final String ASSIGNMENTS_NOT_NULL = "assessments must not be null";

  /** Message when a {@code assignments} parameter is an empty collection. */
  public static final String ASSIGNMENTS_NOT_EMPTY = "assessment must not be empty";

  /**
   * Make a validation message when an {@code elementName} is too long.
   * 
   * @param elementName the name of the parameter or attribute
   * 
   * @return the validation message
   */
  public static final String makeTooLongMessage(final String elementName) {
    return elementName + " is too long";
  }

  /**
   * Make a validation message when an {@code elementName} is too short.
   * 
   * @param elementName the name of the parameter or attribute
   * 
   * @return the validation message
   */
  public static final String makeTooShortMessage(final String elementName) {
    return elementName + " is too short";
  }

  //
  // Services
  //

  //
  // CandidateService
  //

  /** Message when a {@code candidate} parameter is null. */
  public static final String CANDIDATE_NOT_NULL = "candidate must not be null";

  /** Message when a {@code candidate} parameter does not exist in the database. */
  public static final String CANDIDATE_DOESNT_EXIST = "candidate must have a database id";


  /** Message when a {@code user} parameter does not exist in the database. */
  public static final String USER_DOESNT_EXIST = "user must have a database id";


  /** Message when a {@code text} parameter is null. */
  public static final String TEXT_NOT_NULL = "text must not be null";


  /** Message when a {@code lastName} parameter is null. */
  public static final String LAST_NOT_NULL = "lastName must not be null";

  /** Message when a {@code recruiter} parameter is null. */
  public static final String RECRUITER_NOT_NULL = "recruiter must not be null";

  //
  // EmailService
  //

  /** Message when a {@code assignment} parameter is null. */
  public static final String ASSIGNMENT_NOT_NULL = "assignment must not be null";

  /** Message when a {@code assignment's person} parameter is null. */
  public static final String PERSON_NOT_NULL = "assignment's person must not be null";

  /** Message when a {@code assignment's assessment} parameter is null. */
  public static final String ASSESSEMENT_NOT_NULL = "assignment's assessment must not be null";



  /**
   * Make a validation message when the email settings for an assessment
   * does not exist in the database.
   * 
   * @param assessmentName the name of the {@link Assessment}
   * 
   * @return the validation message
   */
  public static final String makeEmailSettingsMessage(final String assessmentName) {
    return "No email settings found for the '" + assessmentName + "' assessment.";
  }

  //
  // UserService
  //

  /** Message when a {@code userTemplate} parameter is null. */
  public static final String TEMPLATE_NOT_NULL = "userTemplate must not be null";

  /** Message when a {@code userTemplate firstName} parameter is null. */
  public static final String FIRSTNAME_NOT_NULL = "userTemplate firstName must not be null";

  /** Message when a {@code userTemplate lastName} parameter is null. */
  public static final String LASTNAME_NOT_NULL = "userTemplate lastName must not be null";

  /** Message when a {@code user password} is null. */
  public static final String PASSWORD_NOT_NULL = "user password must not be null";


  /**
   * Make a validation message when an {@code emailAddress} already exists in the database.
   * 
   * @param emailAddress the email address
   * 
   * @return the validation message
   */
  public static final String makeDuplicateEmailMessage(final String emailAddress) {
    return "The email address: " + emailAddress + " is already in use.";
  }

  //
  // ResponseService
  //

  /** Message when a {@code assessment response} parameter is null. */
  public static final String ASSESSMENT_RESPONSE_NOT_NULL = "assessment response must not be null";

  /** Message when a {@code assessment response} parameter is null. */
  public static final String ASSESSMENT_RESPONSE_KEY = "cannot create an entity with a specific key";
}
