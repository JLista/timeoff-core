/**
* Copyright 2015 - 2015: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.util;

import java.util.Arrays;

import org.aspectj.lang.ProceedingJoinPoint;
import org.aspectj.lang.annotation.Around;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Pointcut;
import org.aspectj.lang.reflect.MethodSignature;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;


/**
 * The aspect that performs enter/exit and exception handling logging.
 * 
 * @author <a href='mailto:npadmannavar@solutechnology.com'>Nikita Padmannavar</a>
 */
@Aspect
@Order(Ordered.HIGHEST_PRECEDENCE)
@Component
public class LoggingAspect {
  private static final SoluLogger LOG = SoluLogger.getLogger(LoggingAspect.class);

  /**
   * Symbolic definition of the cut-point for all public Service component methods.
   */
  @Pointcut("execution(public * com.solu.services..*.*(..))")
  public void allServiceMethods() {}

  /**
   * Handler for the around-aspect on all public Service component methods.
   * 
   * @param pjp the wrapped method's join point
   * 
   * @return the value returned by the wrapped method
   * 
   * @throws Throwable any exception thrown by the wrapped method
   */
  @Around("allServiceMethods()")
  public Object captureExceptions(final ProceedingJoinPoint pjp) throws Throwable {
    String methodName = getMethodName(pjp);
    try {
      // log entry
      if (LOG.isTraceEnabled()) {
        LOG.logTrace("Entering %s: %s", methodName, Arrays.toString(pjp.getArgs()));
      } else {
        LOG.logDebug("Entering %s", methodName);
      }
      
      // delegate to the real method
      Object result = pjp.proceed();
      
      // log exit
      if (LOG.isTraceEnabled()) {
        LOG.logTrace("Exiting %s: %s", methodName, (doesMethodReturnVoid(pjp) ? "void" : result));
      } else {
        LOG.logDebug("Exiting %s", methodName);
      }
      
      // return the result
      return result;
      
    }
    // capture any exception and log it
    catch (Throwable exception) {
      final String message = makeErrorMessage(methodName, exception);
      LOG.logError(message, exception);
      throw exception;
    }
  }

  //
  // Private methods
  //

  /**
   * Gets the name (as a Class.Method pair) of the wrapped method.
   * 
   * @param pjp the wrapped method's join point
   * 
   * @return the name of the wrapped method
   */
  private String getMethodName(final ProceedingJoinPoint pjp) {
    MethodSignature signature = (MethodSignature) pjp.getSignature();
    return signature.getDeclaringType().getSimpleName() + "." + signature.getName();
  }

  /**
   * Queries whether of the wrapped method returns {@code void}.
   * 
   * @param pjp the wrapped method's join point
   * 
   * @return {@code true} if the wrapped method is {@code void}; otherwise {@code false}
   */
  private boolean doesMethodReturnVoid(final ProceedingJoinPoint pjp) {
    MethodSignature signature = (MethodSignature) pjp.getSignature();
    Class<?> returnType = signature.getReturnType();
    return Void.TYPE.equals(returnType);
  }

  /**
   * Makes a generic error message to be logged when the wrapped method fails.
   * 
   * @param methodName the method name of the wrapped method
   * @param exception the exception thrown by the wrapped method
   * 
   * @return the generic error message
   */
  private String makeErrorMessage(String methodName, Throwable exception) {
    return String.format("The %s method failed because '%s'.", methodName, exception.getMessage());
  }

}
