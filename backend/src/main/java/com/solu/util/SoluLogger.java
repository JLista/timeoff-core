/**
* Copyright 2015 - 2015: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.util;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * A Decorator on the log4j {@link Logger} class that supports string
 * formatting.
 * 
 * <p>The levels are (in order):
 * <ul>
 *   <li>ERROR</li>
 *   <li>WARNING</li>
 *   <li>INFO</li>
 *   <li>DEBUG</li>
 *   <li>TRACE</li>
 * </ul>
 *
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
public final class SoluLogger {

  //
  // Static factory methods
  //

  /**
   * Create a new logger for a specific Java class.
   */
  public static SoluLogger getLogger(final Class<?> clazz) {
    return new SoluLogger(LoggerFactory.getLogger(clazz));
  }

  //
  // Attributes
  //

  /**
   * The decorated log4j {@link Logger}.
   */
  private final Logger logger;

  //
  // Constructor
  //

  /**
   * Construct a new decorated logger.
   * 
   * @param logger
   *          The log4j {@link Logger} to be decorated.
   */
  private SoluLogger(final Logger logger) {
    this.logger = logger;
  }

  //
  // Public methods
  //

  /**
   * Log an error message with no exception.
   * 
   * @param msg
   *          The msg that might include
   *          {@link String#format(String, Object...)} arguments.
   * @param msgArgs
   *          An optional collection of format arguments.
   */
  public final void logError(final String msg, final Object... msgArgs) {
    if (logger.isErrorEnabled()) {
      logger.error(String.format(msg, msgArgs));
    }
  }

  /**
   * Log an error message with an exception.
   * 
   * @param t
   *          The exception to be logged.
   * @param msg
   *          The msg that might include
   *          {@link String#format(String, Object...)} arguments.
   * @param msgArgs
   *          An optional collection of format arguments.
   */
  public final void logError(final Throwable t, final String msg, final Object... msgArgs) {
    if (logger.isErrorEnabled()) {
      logger.error(String.format(msg, msgArgs), t);
    }
  }

  /**
   * Log a warning message.
   * 
   * @param msg
   *          The msg that might include
   *          {@link String#format(String, Object...)} arguments.
   * @param msgArgs
   *          An optional collection of format arguments.
   */
  public final void logWarning(final String msg, final Object... msgArgs) {
    if (logger.isWarnEnabled()) {
      logger.warn(String.format(msg, msgArgs));
    }
  }

  /**
   * Is the logger instance enabled for the INFO level?
   *
   * @return {@code true} if this Logger is enabled for the INFO level,
   *         {@code false} otherwise.
   */
  public final boolean isInfoEnabled() {
    return logger.isInfoEnabled();
  }

  /**
   * Log an informational message.
   * 
   * @param msg
   *          The msg that might include
   *          {@link String#format(String, Object...)} arguments.
   * @param msgArgs
   *          An optional collection of format arguments.
   */
  public final void logInfo(final String msg, final Object... msgArgs) {
    if (logger.isInfoEnabled()) {
      logger.info(String.format(msg, msgArgs));
    }
  }

  /**
   * Is the logger instance enabled for the DEBUG level?
   *
   * @return {@code true} if this Logger is enabled for the DEBUG level,
   *         {@code false} otherwise.
   */
  public final boolean isDebugEnabled() {
    return logger.isDebugEnabled();
  }

  /**
   * Log a debug message.
   * 
   * @param msg
   *          The msg that might include
   *          {@link String#format(String, Object...)} arguments.
   * @param msgArgs
   *          An optional collection of format arguments.
   */
  public final void logDebug(final String msg, final Object... msgArgs) {
    if (logger.isDebugEnabled()) {
      logger.debug(String.format(msg, msgArgs));
    }
  }

  /**
   * Is the logger instance enabled for the TRACE level?
   *
   * @return {@code true} if this Logger is enabled for the TRACE level,
   *         {@code false} otherwise.
   */
  public final boolean isTraceEnabled() {
    return logger.isTraceEnabled();
  }

  /**
   * Log a trace message.
   * 
   * @param msg
   *          The msg that might include
   *          {@link String#format(String, Object...)} arguments.
   * @param msgArgs
   *          An optional collection of format arguments.
   */
  public final void logTrace(final String msg, final Object... msgArgs) {
    if (logger.isTraceEnabled()) {
      logger.trace(String.format(msg, msgArgs));
    }
  }

}
