/**
* Copyright 2015 - 2016: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.domain;

import java.io.IOException;

import com.fasterxml.jackson.core.JsonGenerator;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonSerializer;
import com.fasterxml.jackson.databind.SerializerProvider;

/**
 * The JSON serializer for the {@link Role} enum.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
public class RoleSerializer extends JsonSerializer<Role> {

  /**
   * Method that can be called to ask implementation to serialize
   * values of type this serializer handles.
   *
   * @param value Value to serialize; can <b>not</b> be null.
   * @param jgen Generator used to output resulting Json content
   * @param provider Provider that can be used to get serializers for
   *   serializing Objects value contains, if any.
   */
  @Override
  public void serialize(final Role value, final JsonGenerator jgen, final SerializerProvider provider)
      throws IOException, JsonProcessingException {
    jgen.writeStartObject();
    jgen.writeFieldName("code");
    jgen.writeString(value.name());
    jgen.writeFieldName("displayName");
    jgen.writeString(value.getDisplayName());
    jgen.writeFieldName("description");
    jgen.writeString(value.getDescription());
    jgen.writeFieldName("roleSubsumed");
    final String roleSubsumedName;
    Role roleSubsumed = value.getRoleSubsumed();
    if (roleSubsumed != null) {
      roleSubsumedName = roleSubsumed.name();
    } else {
      roleSubsumedName = "";
    }
    jgen.writeString(roleSubsumedName);
    jgen.writeEndObject();
  }

}
