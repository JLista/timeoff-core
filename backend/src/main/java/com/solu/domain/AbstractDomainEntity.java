/**
* Copyright 2015 - 2016: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.domain;

import java.io.Serializable;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Objects;
import com.google.common.base.Objects.ToStringHelper;


/**
 * An abstract base class for all Domain entity components.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
@SuppressWarnings("serial")
public abstract class AbstractDomainEntity<K extends Serializable> implements DomainEntity<K> {

  //
  // DomainEntity methods
  //

  /**
   * {@inheritDoc}
   */
  @Override
  @JsonIgnore
  public final boolean isNew() {
    return getId() == null;
  }

  //
  // Object methods
  //

  /**
   * Returns a hash code based upon the entity's database id.
   * <p>
   * {@inheritDoc}
   */
  @Override
  public int hashCode() {
    return isNew() ? 0 : this.getId().hashCode();
  }

  //
  // Helper methods
  //

  /**
   * Helper method for the {@link #equals(Object)} method on all Entity components.
   * 
   * <p>
   * Used like this:
   * <pre>
   * &commat;Entity
   * public class Assessment extends AbstractDomainEntity&lt;Integer&gt; {
   *   // code not shown
   *   &commat;Override
   *   public boolean equals(Object obj) {
   *     return equals(obj, Assessment.class);
   *   }
   * }
   * </pre>
   * </p>
   * 
   * @param <E> The type of the Entity being compared.
   * 
   * @param obj  The object to compare against {@code this} object.
   * @param entityType  The {@link Class} object of that Entity type.
   * 
   * @return {@code true} if the {@code obj} has the same type and has the same DB ID
   */
  protected <E extends DomainEntity<?>> boolean equals(Object obj, final Class<E> entityType) {
    if (obj == this) return true;
    if (obj == null) return false;
    // NOTE: This is the preferable check because JPA creates Entity wrappers
    // around our Entity components.  The problem with this check is that it
    // allows subclasses of Entity classes to be considered equal if they
    // share the same DB ID.
    if (!entityType.isAssignableFrom(obj.getClass())) return false;
    E that = entityType.cast(obj);
    return Objects.equal(this.getId(), that.getId());
  }
  
  /**
   * 
   * Helper method for the {@link #toString()} method on all Entity components.
   * 
   * <p>
   * Used like this:
   * <pre>
   * &commat;Entity
   * public class Assessment extends AbstractDomainEntity&lt;Integer&gt; {
   *   // code not shown
   *   &commat;Override
   *   public String toString() {
   *     return toStringHelper(Assessment.class).add("name", name).toString();
   *   }
   * }
   * </pre>
   * </p>
   * 
   * @param <E> The type of the Entity being compared.
   * 
   * @param entityType  The {@link Class} object of that Entity type.
   * 
   * @return The {@link ToStringHelper} for additional 
   */
  protected <E extends DomainEntity<?>> ToStringHelper toStringHelper(final Class<E> entityType) {
    return Objects.toStringHelper(entityType)
        .omitNullValues()
        .add("id", getId())
        .add("isNew", isNew() ? Boolean.TRUE : null);
  }
}
