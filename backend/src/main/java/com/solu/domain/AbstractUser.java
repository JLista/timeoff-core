/**
* Copyright 2015: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.domain;

import static com.solu.util.Preconditions.checkNotNull;
import static com.solu.util.Preconditions.checkTooLong;
import static com.solu.util.ValidationMessages.EMAIL_NOT_NULL;
import static com.solu.util.ValidationMessages.LASTSESSION_NOT_NULL;
import static com.solu.util.ValidationMessages.ROLE_NOT_NULL;

import java.sql.Timestamp;
import java.util.Date;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import javax.persistence.Column;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.MappedSuperclass;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonView;


/**
 * Any user.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
@MappedSuperclass
public abstract class AbstractUser extends AbstractDomainEntity<Integer> {
  private static final long serialVersionUID = 2752782995930098427L;

  //
  // Inner Types
  //

  /**
   * The marker interface used by the JSON conversion tool to extract only
   * those properties that are used to distinguish a user.
   */
  public interface UserView {}

  //
  // Constants
  //

  public static final int MAX_NAME = 30;
  public static final int MAX_EMAIL_ADDR = 255;
  public static final int MAX_PHONE = 15;

  static final String UNKNOWN_PASSWORD = "UNKNOWN";
  
  //
  // Attributes
  //

  @Id
  @GeneratedValue(strategy = GenerationType.AUTO)
  @Column(name = "id")
  private Integer id;

  @Column(name = "firstname", nullable = false, length = MAX_NAME)
  private String firstName;

  @Column(name = "lastname", nullable = false, length = MAX_NAME)
  private String lastName;

  @Column(name = "email_address", nullable = false, length = MAX_EMAIL_ADDR)
  private String emailAddress;

  @Column(name = "enabled", nullable = false)
  private boolean enabled = true;

  @Enumerated(EnumType.STRING)
  private Role role;

  @Column(name = "last_session", nullable = true)
  private Date lastSession;

  @Column(name = "created_on")
  protected Timestamp createDate;

  @Column(name = "updated_on")
  protected Timestamp modifyDate;

  //
  // Constructors
  //

  /**
   * The no-arg ctor for Serialization and JPA.
   */
  protected AbstractUser() {}

  //
  // DomainEntity methods
  //

  @JsonView(AbstractUser.UserView.class)
  @Override
  public Integer getId() {
    return id;
  }
  // no setter; DB generated

  //
  // Accessors
  //

  @JsonView(AbstractUser.UserView.class)
  public String getFirstName() {
    return firstName;
  }
  @JsonView(AbstractUser.UserView.class)
  public void setFirstName(String firstName) {
    this.firstName = firstName;
  }

  @JsonView(AbstractUser.UserView.class)
  public String getLastName() {
    return lastName;
  }
  @JsonView(AbstractUser.UserView.class)
  public void setLastName(String lastName) {
    this.lastName = lastName;
  }
  @JsonView(AbstractUser.UserView.class)
  public String getFullName() {
    final String fullName = (firstName + " " + lastName).trim();
    return !fullName.isEmpty() ? fullName : this.emailAddress;
  }

  //
  // User methods
  //

  @JsonView(AbstractUser.UserView.class)
  public String getEmailAddress() {
    return emailAddress;
  }
  @JsonView(AbstractUser.UserView.class)
  public void setEmailAddress(String emailAddress) {
    // validations
    checkNotNull(emailAddress, EMAIL_NOT_NULL);
    checkTooLong(emailAddress, MAX_EMAIL_ADDR, "email");
    //
    this.emailAddress = emailAddress;
  }

  @JsonView(AbstractUser.UserView.class)
  public boolean isEnabled() {
    return enabled;
  }
  @JsonView(AbstractUser.UserView.class)
  public void setEnabled(final boolean enabledIn) {
    this.enabled = enabledIn;
  }

  @JsonIgnore
  public String getPassword() {
    // Must always return a non-null pw to satisfy Spring Security.
    // This happens when the user has signed-in with a Social media site, such as Google.
    return UNKNOWN_PASSWORD;
  }

  @JsonIgnore
  public Role getRole() {
    return role;
  }
  @JsonIgnore
  public void setRole(Role role) {
    this.role = checkNotNull(role, ROLE_NOT_NULL);
  }

  @JsonView(AbstractUser.UserView.class)
  public String getRoleCode() {
    return role.name();
  }
  @JsonView(AbstractUser.UserView.class)
  public void setRoleCode(final String roleCode) {
    this.role = Role.roleOf(roleCode);
  }

  public boolean hasRole(Role role) {
    Role nextRole = this.role;
    do {
      if (nextRole.equals(role)) return true;
      nextRole = nextRole.getRoleSubsumed();
    } while (nextRole != null);
    return false;
  }

  @JsonIgnore
  public Set<Role> getAllRoles() {
    final Set<Role> allRoles = new HashSet<>();
    Role nextRole = this.role;
    do {
      allRoles.add(nextRole);
      nextRole = nextRole.getRoleSubsumed();
    } while (nextRole != null);
    return allRoles;
  }

  @JsonView(AbstractUser.UserView.class)
  public Set<String> getAllRoleCodes() {
    return getAllRoles()
        .stream()
        .map(role -> role.name())
        .collect(Collectors.toSet());
  }

  @JsonProperty
  @JsonView(AbstractUser.UserView.class)
  public Date getLastSession() {
    return lastSession;
  }
  @JsonIgnore
  public void setLastSession(final Date lastSession) {
    this.lastSession = checkNotNull(lastSession, LASTSESSION_NOT_NULL);
  }

  //
  // Object methods
  //

  @Override
  public boolean equals(Object obj) {
    return equals(obj, AbstractUser.class);
  }

  @Override
  public String toString() {
    return toStringHelper(AbstractUser.class).toString();
  }
}
