/**
* Copyright 2015 - 2016: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
*/
package com.solu.domain;

import java.io.Serializable;

/**
 * Generic contract for Domain Entity components.
 * 
 * <p>
 * A Domain Entity has an id that is populated by the database on the create
 * operation.  The id should never be set by any other code component; thus
 * this interface has a {@link #getId()} method but not a {@code setId} method.
 * </p>
 * 
 * <p>
 * The interface also includes a {@link #isNew()} method that queries the entity
 * instance whether it has been created in the database or not; the details of 
 * that check should remain hidden behind the interface and not exposed to other
 * components.  For example, there used by the code that did a check like this:
 * {@code entity.getId() == 0} to determine if the entity was new.  This is a bad
 * practice and this interface creates a consistent, clean API to use.
 * </p>
 * 
 * @param <K> the type of the database id, usually an {@link Integer} or {@link Long}
 *   or {@link String} but it could also be a composite key; it just needs to be
 *   {@link Serializable}.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
public interface DomainEntity<K extends Serializable> extends Serializable {

  /**
   * Shared validation message that the Entity already exists in the DB.
   */
  static final Object ENTITY_ALREADY_EXISTS = "This entity already exists.";

  /**
   * Shared validation message that the Entity doesn't exist in the DB.
   */
  static final Object ENTITY_DOESNT_EXIST = "This entity doesn't exist.";

  /**
   * The attribute name of the database id.
   * 
   * <p>
   * This may be used by testing code to manually inject dummy DB ids into entities.
   * </p>
   */
  static final String ID_FIELD = "id";

  /**
   * Retrieve the database id for this Entity.
   * 
   * @return The database id.
   */
  K getId();

  /**
   * Queries whether this Entity is new (no DB ID).
   * 
   * @return {@code true} if the Entity is new; otherwise {@code false}
   */
  boolean isNew();
}
