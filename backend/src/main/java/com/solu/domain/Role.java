/**
* Copyright 2015 - 2016: Solu Technology Partners
* All Rights Reserved. Proprietary and Confidential information of Solu Technology Partners
* Disclosure, Use or Reproduction without the written authorization of Solu Technology Partners is prohibited
 */
package com.solu.domain;

import static com.solu.util.Preconditions.checkArgument;
import static com.solu.util.Preconditions.checkNotNull;

import java.util.List;
import java.util.Map;

import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import com.google.common.collect.ImmutableList;
import com.google.common.collect.Maps;

/**
 * The Role class provides an enumeration of all possible user roles.
 * 
 * @author <a href='mailto:bbasham@solutechnology.com'>Bryan Basham</a>
 */
@JsonSerialize(using = RoleSerializer.class)
public enum Role {

  /** Generic user role. */
  ROLE_USER(null, "Guest", "A guest has no fundamental privledges."),

  /** Candidates may edit their profile. */
  ROLE_CANDIDATE(ROLE_USER, "Candidate",
      "A candidate has the right to view and update their profile."),

  /** Staff may edit their profile. */
  ROLE_STAFF(ROLE_CANDIDATE, "Staff",
      "A person that has been hired (or contracted)."),

  /** Recruiters may create and manage candidates and assign assessments. */
  ROLE_RECRUITER(ROLE_STAFF, "Recruiter",
      "Add candidates, send assessments but no access for test modifications."),

  /** SMT administrator. */
  ROLE_ADMIN(ROLE_RECRUITER, "Administrator", "Full Access to website"),

  /** Client users have access to Client-specific use cases. */
  ROLE_CLIENT(ROLE_USER, "Client", "A Client of Solu");

  //
  // Public
  //

  /**
   * Gets an immutable {@linkplain List list} of all roles.
   */
  public static List<Role> getAll() {
    return ALL;
  }
  
  /**
   * Gets a {@link Role} value from its {@code roleCode}.
   * 
   * @param roleCode  the name of a {@link Role} value
   * 
   * @return the {@link Role} value
   * 
   * @throws NullPointerException
   *    when the {@code roleCode} parameter is null
   * @throws IllegalArgumentException
   *    when the {@code roleCode} parameter does not match a role name
   */
  public static Role roleOf(final String roleCode) {
    checkNotNull(roleCode, "roleCode must not be null");
    checkArgument(ALL_MAP.containsKey(roleCode), "'%s' is not a valid Role code", roleCode);
    return ALL_MAP.get(roleCode);
  }

  /**
   * Gets the role that this role subsumes.
   * 
   * Could be {@code null} this role is a root.
   */
  public Role getRoleSubsumed() {
    return subsumes;
  }

  /**
   * Gets the user-friend name for this role.
   */
  public String getDisplayName() {
    return displayName;
  }

  /**
   * Gets the user-friend description for this role.
   */
  public String getDescription() {
    return description;
  }

  //
  // Private
  //

  private final String displayName;
  private final String description;
  private final Role subsumes;

  private static final List<Role> ALL =
      ImmutableList.copyOf(values());
  private static final Map<String, Role> ALL_MAP =
      Maps.uniqueIndex(ALL, role -> role.name());

  /**
   * The private ctor for Role enum values.
   * 
   * @param subsumes
   *    the {@link Role} that this role inherits from
   * @param displayName
   *    the user-friend name for this role
   * @param description
   *    the user-friend description for this role
   */
  private Role(final Role subsumes, final String displayName, final String description) {
    this.subsumes = subsumes;
    this.displayName = displayName;
    this.description = description;
  }
}
